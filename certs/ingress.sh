#!/bin/bash
rm ingress-argocd.yaml -f

# Create TLS certs
openssl req -x509 -sha512 -nodes -days 365 -newkey rsa:4096 -keyout tls.key -out tls.crt -subj "//SKIP/C=US/ST=U.S Government/O=DOD/O=PKI/CN=argocd.localdev.me"

# Create TLS Secret
cat <<EOT >> ingress-argocd.yaml
apiVersion: v1
kind: Secret
metadata:
  name: app-tls
  namespace: argocd
type: kubernetes.io/tls
data:
  tls.crt: $(cat tls.crt | base64 -w 0)
  tls.key: $(cat tls.key | base64 -w 0)
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: argocd-localhost
  namespace: argocd
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: 'true'
    nginx.ingress.kubernetes.io/ssl-passthrough: 'true'
    nginx.ingress.kubernetes.io/backend-protocol: 'HTTPS'
spec:
  rules:
    - host: argocd.localdev.me
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: argocd-server
                port:
                  name: https
  tls:
    - hosts:
        - argocd.localdev.me
      secretName: app-tls
EOT

kubectl apply -f ingress-argocd.yaml