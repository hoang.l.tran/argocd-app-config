#!/bin/bash
kubectl delete ns elastic
sleep 30

kubectl create ns elastic

sh ./ingress.sh

kubectl apply -f ./elasticsearch.yaml
sleep 30

kubectl apply -f ./kibana.yaml
sleep 30

kubectl apply -f ./apm-server.yaml

sleep 60


echo

#kubectl exec -it svc/elasticsearch -n elastic -- ./bin/elasticsearch-certutil csr -name https -dns elasticsearch.localdev.me

kubectl exec -it svc/elasticsearch -n elastic -- ./bin/elasticsearch-create-enrollment-token --scope kibana

read -p "Press any key to resume ..."

echo

kubectl logs svc/kibana -n elastic


read -p "Press any key to exit!"
