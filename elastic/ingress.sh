#!/bin/bash
rm elasticsearch-tls-secret.yaml -f

# Create TLS certs
openssl req -x509 -sha512 -nodes -days 365 -newkey rsa:4096 -keyout tls.key -out tls.crt -subj "//SKIP/C=US/ST=U.S Government/O=DOD/CN=elasticsearch.localdev.me"

# Create TLS Secret
cat <<EOT >> elasticsearch-tls-secret.yaml
apiVersion: v1
kind: Secret
metadata:
  name: elasticsearch-tls-secret
  namespace: elastic
type: kubernetes.io/tls
data:
  tls.crt: $(cat tls.crt | base64 -w 0)
  tls.key: $(cat tls.key | base64 -w 0)
EOT

kubectl apply -f elasticsearch-tls-secret.yaml

kubectl apply -f ./secret.yaml


sleep 5